Ngo.delete_all
x = Ngo.new
x.name = "BRLP"
x.save
x = Ngo.new
x.name = "CRY"
x.save
x = Ngo.new
x.name = "Sanjeevani"
x.save

Vaccination.delete_all
x = Vaccination.new
x.remID = 1
x.dab = -135
x.message = 'Take first dose tetanus vaccine'
x.audio_url = "http://s3.amazonaws.com/bpsevaplus/Unicasts/username/rem001.mp3"
x.save

x = Vaccination.new
x.remID = 2
x.dab = -111
x.message = 'Take second dose of tetanus vaccine'
x.audio_url = "http://s3.amazonaws.com/bpsevaplus/Unicasts/username/rem002.mp3"
x.save

#after birth. assuming it starts from here. 
x = Vaccination.new
x.remID = 3
x.dab = 7
x.message = 'Take BCG ; OPV zero ; Hepatitis B-1 vaccine'
x.audio_url = "http://s3.amazonaws.com/bpsevaplus/Unicasts/username/rem003.mp3"
x.save

x = Vaccination.new
x.remID = 4
x.dab = 42 # 6 weeks
x.message = 'Take OPV-1 + IPV-1 / OPV -1 vaccine, OPV alone if IPV cannot be given ; DTPw-1/DTPa-1 ; Hepatitis B-2 ; Hib-1'
x.audio_url = "http://s3.amazonaws.com/bpsevaplus/Unicasts/username/rem004.mp3"
x.save

x = Vaccination.new
x.remID = 5
x.dab = 60
x.message = 'Take OPV-2 + IPV-2 / OPV-2 OPV alone if IPV cannot be given ; DTPw-2/DTPa-2 ; Hib-2'
x.audio_url = "http://s3.amazonaws.com/bpsevaplus/Unicasts/username/rem005.mp3"
x.save

x = Vaccination.new
x.remID = 6
x.dab = 88
x.message = 'Take OPV-3 + IPV-3 / OPV-3 OPV alone if IPV cannot be given ; DTPw-1/DTPa-1 ; Hepatitis B-3 ; Hib-3'
x.audio_url = "http://s3.amazonaws.com/bpsevaplus/Unicasts/username/rem006.mp3"
x.save

x = Vaccination.new
x.remID = 7
x.dab = 275
x.message = 'Take Measles vaccine'
x.audio_url = "http://s3.amazonaws.com/bpsevaplus/Unicasts/username/rem007.mp3"
x.save

x = Vaccination.new
x.remID = 8
x.dab = 393
x.message = 'Take OPV-4 + IPV-B1 / OPV-4 OPV alone if IPV cannot be given ;  DTPw booster-1/DTPa booster-1 ; Hib booster ; MMR-1'
x.audio_url = "http://s3.amazonaws.com/bpsevaplus/Unicasts/username/rem008.mp3"
x.save

x = Vaccination.new
x.remID = 9
x.dab = 730
x.message = 'Take typhoid vaccine'
x.audio_url = "http://s3.amazonaws.com/bpsevaplus/Unicasts/username/rem009.mp3"
x.save

x = Vaccination.new
x.remID = 10
x.dab = 1825
x.message = 'Take OPV-5 ; DTPw-1/DTPa-1 ; DTPw booster-2/DTPa booster-2 ; MMR-2'
x.audio_url = "http://s3.amazonaws.com/bpsevaplus/Unicasts/username/rem010.mp3"
x.save

x = Vaccination.new
x.remID = 11
x.dab = 2190
x.message = 'Take typhoid vaccine'
x.audio_url = "http://s3.amazonaws.com/bpsevaplus/Unicasts/username/rem011.mp3"
x.save

x = Vaccination.new
x.remID = 12
x.dab = 3650
x.message = 'Take Tdap ; HPV (only girls)'
x.audio_url = "http://s3.amazonaws.com/bpsevaplus/Unicasts/username/rem012.mp3"
x.save
