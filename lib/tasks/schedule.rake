task :scheduler => :environment do
	STDOUT.sync = true
	print "Scheduler Starting\n"
	
	loop do
			print "Scheduler running " , Time.now , "\n"
			
			# DO unicasts
			t = Time.now			
			list = Reminders.where(:done => 0)
			
			#list = Reminders.where(:datetime.gte => t , :datetime.lte => t + 1.minutes,:done => 0)
			print "UNICAST list: \n",list.to_a,"\n"
			list.each do |unicast|
				
				user = User.where(:username => unicast.username).first
				if (user.uni_sms_voice == 'sms') ## sms
					per = Person.where(:personid => unicast.pid).first
					message = user.usersetting.sms[unicast.remID.to_s]
					Delayed::Job.enqueue(SmsSender.new(per.mobnum.to_s[-10,10],message))
				else  ## voice
					per = Person.where(:personid => unicast.pid).first
					audio_url = user.usersetting.voice[unicast.remID.to_s]
					print audio_url
	
					Delayed::Job.enqueue(CallMaker.new(per,unicast.serviceID,audio_url))
				end

				unicast.done = 1 # unicast deployed
				unicast.save
			end


			# Do Multicasts
			t = Time.now			
			list = Multicasts.where(:datetime.gte => t , :datetime.lte => t + 1.minutes,:active => 1)
			print  "\nMulticast list: \n",list.to_a,"\n"
			list.each do |multicast|
				
				if (multicast.sms_voice == "voice") # CALL

					multicast.people_ids.each do |per|
						Delayed::Job.enqueue(CallMaker.new(per,multicast.id,multicast.audio.url))
					end

				else # SMS
					multicast.people_ids.each do |per|
						p =  Person.where(:personid => per).first
						if (p)
							Delayed::Job.enqueue(SmsSender.new(p.mobnum.to_s[-10,10],multicast.message))
						else
							print "\nERROR : MULTICAST_SMS : Person with pid : ",per, " doesn't exist\n"
						end
					end
				end

				multicast.active = 2 # Multicast deployed
				multicast.save
			end


			## All work done sleep 

			print "\nGonna sleep now for 20 secs\n"
			sleep (20) # 1 minutes
	end

end