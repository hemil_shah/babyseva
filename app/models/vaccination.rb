class Vaccination
  include Mongoid::Document
  include Mongoid::MultiParameterAttributes 

  field :remID , :type => Integer
  field :dab , :type => Integer # dab =>  Days after birth  (- 9months x 30days) to (10 years x 365 days) = -270 to 3650
  
  field :message , :type => String ,:default => "Message"
  field :audio_url ,:type => String ,:default => "http://s3.amazonaws.com/sevaplus/Unicasts/username/rem001.mp3"

  validates_presence_of :remID
  validates_uniqueness_of :remID

end


## This class stores the list of all the reminders 
## it is populated by running db:seed