class Usersetting
  include Mongoid::Document
  include Mongoid::MultiParameterAttributes
  
  belongs_to :user

  field :username 
  
  field :sms # Hash of sms reminders
  field :voice # of voice urls
  field :dabs # days after birth


end


## this class stores reminder data about individual user
## like the sms text , voice links 
