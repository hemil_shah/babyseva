class HomeController < ApplicationController
	before_filter :authenticate_user! ,:except => [:index,:features,:contactus,:howtouse]

	def index

	end

	def home
		@famcount = Family.where(:username=> current_user.username).count
		@peoplecount = Person.where(:username=> current_user.username).count
		@multicount = Multicasts.where(:username=>current_user.username).count
	end

	def features
	

	end

	def contactus
	
	end
	
	def calender
		render :calender , :layout => false
	end
end
