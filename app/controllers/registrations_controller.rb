class RegistrationsController < Devise::RegistrationsController
  
  def after_sign_up_path_for(resource)
 	#SEND A welcome mail
   Usermailer.delay.welcomemail(resource)
   x = Usersetting.new
   x.username = resource.username
   createUserSettings(x)
   resource.usersetting = x
   x.save
   resource.save
   '/'
  end
  

  def createUserSettings(x)
  	v = Vaccination.all
  	dabsHash = Hash.new
  	smsHash= Hash.new
  	voiceHash = Hash.new
  	v.each do |vac|
  		dabsHash[vac.remID] = vac.dab
  		smsHash[vac.remID] = vac.message
  		voiceHash[vac.remID] = vac.audio_url
  	end
  	x.dabs = dabsHash
  	x.sms = smsHash
  	x.voice = voiceHash
  	x.save
  end

end